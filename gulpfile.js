var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix){

 // mix.scripts([    
 //         "./node_modules/jquery/dist/jquery.min.js"
 //     ],'./resources/assets/js/vendor.js');

    // mix.styles([
    //      "./node_modules/bootstrap/dist/css/bootstrap.min.css",
    //  ],'./resources/assets/css/vendor.css');

	// mix.copy('./resources/assets/css/vendor.css', './public/vendor/css/vendor.css');
    //mix.copy('./node_modules/gsap/src/minified/TweenMax.min.js', './public/vendor/js/TweenMax.min.js');
   mix.copy('./node_modules/plyr/dist/plyr.js','./public/vendor/js/plyr.js');
   mix.copy('./node_modules/plyr/dist/plyr.css','./public/vendor/css/plyr.css');
   mix.copy('./node_modules/plyr/dist/plyr.svg','./public/vendor/img/videos/plyr.svg');



} )


require('laravel-elixir-livereload');
 
elixir(function (mix) {
    mix.livereload();
});