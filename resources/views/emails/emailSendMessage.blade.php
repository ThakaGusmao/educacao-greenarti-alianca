<!doctype html>
<html style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
<head style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
    <meta name="viewport" content="width=device-width" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic" rel="stylesheet" type="text/css" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
    <title style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">Alianca de Minas</title>
    <style type="text/css" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
/* -------------------------------------
    GLOBAL
    ------------------------------------- */
    * {
        font-family: "Didact Gothic", "Helvetica", Helvetica, Arial, sans-serif;
        font-size: 100%;
        line-height: 1.6em;
        margin: 0;
        padding: 0;
    }

    img {
       width: 100%;
    height: 100%;
    }

    body {
        -webkit-font-smoothing: antialiased;
        height: 100%;
        -webkit-text-size-adjust: none;
        width: 100% !important;
    }


/* -------------------------------------
    ELEMENTS
    ------------------------------------- */
    a {
        color: #348eda;
    }

    .btn-primary {
        Margin-bottom: 10px;
        width: auto !important;
    }

    .btn-primary td {
        background-color: #348eda; 
        border-radius: 25px;
        font-family: "Didact Gothic", Helvetica, Arial, "Lucida Grande", sans-serif; 
        font-size: 14px; 
        text-align: center;
        vertical-align: top; 
    }

    .btn-primary td a {
        background-color: #348eda;
        border: solid 1px #348eda;
        border-radius: 25px;
        border-width: 10px 20px;
        display: inline-block;
        color: #ffffff;
        cursor: pointer;
        font-weight: bold;
        line-height: 2;
        text-decoration: none;
    }

    .last {
        margin-bottom: 0;
    }

    .first {
        margin-top: 0;
    }

    .padding {
        padding: 10px 0;
    }


/* -------------------------------------
    BODY
    ------------------------------------- */
    table.body-wrap {
        padding: 20px;
        width: 600px;
        margin-left: auto;
        margin-right: auto;
    }


    table.body-wrap .container {
        border: 1px solid #f0f0f0;
    }
    .border{
        border-bottom: 6px solid #6390B6!important;
        border-radius: 5px;
    }

/* -------------------------------------
    FOOTER
    ------------------------------------- */
    table.footer-wrap {
        clear: both !important;
        width: 100%;  
    }

    .footer-wrap .container p {
        color: #666666;
        font-size: 12px;

    }

    table.footer-wrap a {
        color: #348eda;
    }


/* -------------------------------------
    TYPOGRAPHY
    ------------------------------------- */
    h1, 
    h2, 
    h3 {
        color: #555252;
        font-family: "Didact Gothic", Helvetica, Arial, "Lucida Grande", sans-serif;
        font-weight: 200;
        line-height: 1.2em;
        margin: 40px 0 10px;
    }

    h1 {
        font-size: 36px;
    }
    h2 {
        font-size: 30px;
        font-weight: bold;
    }
    h3 {
        font-size: 30px;
        margin-top: 26px;
        margin-bottom: 5px;
    }
    
    p, 
    ul, 
    ol {
        font-size: 16px!important;
        font-weight: normal;
        margin-bottom: 10px;
        color: #555252;

    }

    ul li, 
    ol li {
        margin-left: 5px;
        list-style-position: inside;
    }

/* ---------------------------------------------------
    RESPONSIVENESS
    ------------------------------------------------------ */

    /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
    .container {
        clear: both !important;
        display: block !important;
        Margin: 0 auto !important;
        max-width: 553px !important;
    }

    /* Set the padding on the td rather than the div for Outlook compatibility */
    .body-wrap .container {
/*      padding: 45px;
*/    }

    /* This should also be a block element, so that it will fill 100% of the .container */
    .content {
        display: block;
        margin: 0 auto;
        max-width: 600px;
        padding: 50px;
        padding-top: 0px;
    }

    /* Let's make sure tables in the content area are 100% wide */
    .content table {
        width: 100%;
    }
    .header{
    width: 100%;
    height: 144px;
    background-color: #dde8f0;
    }

    .text-center{text-align: center;}
    .text-right{text-align: right;}
    .questions{

    padding: 23px;
    border: 1px dashed #6a95b9;
    }
</style>
</head>

<body bgcolor="#f6f6f6" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;-webkit-font-smoothing: antialiased;height: 100%;-webkit-text-size-adjust: none;width: 100% !important;">
    <!-- body -->
    <table class="body-wrap" style="margin-left: auto;margin-right: auto;font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 20px;width: 600px;margin-left: auto;margin-right: auto" bgcolor="#f6f6f6" width="600px" cellpadding="0" cellspacing="0" align="center ">
        <tr style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
            <td class="container border" bgcolor="#FFFFFF" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto !important;padding: 0;border-radius: 5px;border: 1px solid #f0f0f0;border-bottom: 6px solid #6390B6!important;clear: both !important;display: block !important;max-width: 553px !important;">
                <div class="header" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;height: 144px;background-color: #dde8f0;">
                <img src="{{asset('./vendor/img/emails/headerEmail.png')}}" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;height: 100%;">

</div>



                <!-- content -->
                <div class="content" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto;padding: 50px;display: block;max-width: 600px;padding-top: 0px;">
                    <table style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;">
                        <tr style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
                            <td width="600px" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
                                <h3 class="text-center" style="font-family: &quot;Didact Gothic&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;font-size: 27px;line-height: 1.2em;margin: 40px 0 10px;padding: 0;color: #555252;font-weight: 200;margin-top: 26px;margin-bottom: 40px;text-align: center;">{{$data['titleQuestion']}}</h3>
                                <div class="questions" style="padding: 23px;border: 1px dashed #6a95b9;">
                                <h4 class="text-center" style="font-family: &quot;Didact Gothic&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;font-size: 18px;line-height: 0.9em;margin: 40px 0 10px;padding: 0;color: #618eb5;margin-top: 0px;margin-bottom: 10px;text-align: center;">{{$data['title']}}</h4>
                                 <h4 class="text-center" style="font-family: &quot;Didact Gothic&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;font-size: 18px;line-height: 0.9em;margin: 40px 0 10px;padding: 0;color: #618eb5;font-weight: 100;margin-top: 0px;margin-bottom: 5px;text-align: justify;">{{$data['description']}}</h4>
                                  <h4 class="text-center" style="font-family: &quot;Didact Gothic&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;font-size: 18px;line-height: 0.9em;margin: 40px 0 10px;padding: 10px;color: #455b6f;font-weight: bold;margin-top: 0px;margin-bottom: 5px;text-align: justify;">{{$data['response']}}</h4>
                                 </div>
                                <h4 class="text-center" style="font-family: &quot;Didact Gothic&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;font-size: 25px;line-height: 0.8em;margin: 40px 0 10px;padding: 0;color: #555252;font-weight: 200;margin-top: 55px;margin-bottom: 5px;text-align: center;">{{$data['footerQuestion']}}</h4>
                                  <h6 style="text-align:center"><a href="{{ $data['admin'] == false ? route('user.message') : route('admin.message') }}" class="text-center" style="font-family: &quot;Didact Gothic&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif;font-size: 20px;line-height: 0.8em;margin: 40px 0 10px;padding: 0;font-weight: 200;margin-top: 10px;margin-bottom: 0px;text-align: center;">Link de Acesso</a></h6>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- /content -->

            </td>
            <td style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;"></td>
        </tr>
    </table>
    <!-- /body -->

    <!-- footer -->
    <table class="footer-wrap" width="600px" cellpadding="0" cellspacing="0" align="center" style="margin-left: auto;margin-right: auto;font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;clear: both !important;">
        <tr style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
            <td style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;"></td>
            <td class="container" width="600px" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto !important;padding: 0;clear: both !important;display: block !important;max-width: 553px !important;">

                <!-- content -->
                <div class="content" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0 auto;padding: 50px;display: block;max-width: 600px;padding-top: 0px;">
                    <table style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;width: 100%;">
                        <tr style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;">
                            <td align="center" class="text-center" width="600px" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;text-align: center;">
                                <p style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 16px!important;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;"><a href="http://www.aliancademinas.com.br" style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;color: #348eda;">Aliança de Minas - Soluções em Meio Ambiente & Cultura</a>.
                                </p>
                                <p style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 16px!important;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;">   Rua Alvares Cabral, 344, sl, 1002, Lourdes</p>
                                <p style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 16px!important;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;">Belo Horizonte, Minas Gerais</p>
                                <p style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 16px!important;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;">                               Tel: (31) 3226-9616
                                </p>
                                <p style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 16px!important;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;">                               Email: contato@aliancademinas.com.br
                                </p>
                                <p style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 16px!important;line-height: 1.6em;margin: 0;padding: 0;font-weight: normal;margin-bottom: 10px;color: #666666;">                               CNPJ: 08.537.738/0001-02
                                </p>

                            </td>
                        </tr>
                    </table>
                </div>
                <!-- /content -->

            </td>
            <td style="font-family: &quot;Didact Gothic&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;font-size: 100%;line-height: 1.6em;margin: 0;padding: 0;"></td>
        </tr>
    </table>
    <!-- /footer -->

</body>
</html>