<!DOCTYPE html>
<html>
<head>
    <title>Educação - Curso Gestão Cultural</title>
</head>
<link rel="stylesheet" href="{{ asset('/vendor/css/vendor.css') }}"></link>
<link rel="stylesheet" href="{{ asset('/vendor/css/icons.css') }}"></link>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<style type="text/css">

::-webkit-input-placeholder {
   color: #797e8a !important;
   font: 20px 'Open Sans', sans-serif;
   opacity: 1;
}

:-moz-placeholder {
   color: #797e8a !important;
   font: 20px 'Open Sans', sans-serif;
      opacity: 1;

}

::-moz-placeholder {
   color: #797e8a !important;
   font: 20px 'Open Sans', sans-serif;
      opacity: 1;

}

:-ms-input-placeholder {  
   color: #797e8a !important;  
   font: 20px 'Open Sans', sans-serif;
      opacity: 1;

}

        body::-webkit-scrollbar
        {
            width: 8px;
            background-color: transparent;
        }

        body::-webkit-scrollbar-thumb
        {
            background-color: #888;
        }
    body,html{
        margin: 0px;
        padding:0px;
    background: #F6F7FA;
            width: 100%;
        height: 100%;
        font-family: 'Open Sans', sans-serif;

    }
    .ead-login{
            background: #f6f8f8;
    height: 100%;
    }

<body>

    <div class="ead-login">
        @yield('content')

    </div>




    <script type="text/javascript" src="{{ asset('/vendor/js/vendor.js') }}"></script>
        @stack('scripts')

</body>
</html>