@extends('Admin.default')
@section('content')

<style type="text/css">

body::-webkit-scrollbar
{
	width: 8px;
	background-color: transparent;
}

body::-webkit-scrollbar-thumb
{
	background-color: #888;
}
	body,html{
		
		background-color: #ffffff;
		margin-bottom: 0px;
		padding-bottom: 0px;
	}
	.ead-row--ajust--no{
		padding: 0px;
	}
	.ead-questions--row{

		background-color: #FFFFFF;
		border-right: 1px solid #E5E5E5;
		border-top: 1px solid #E5E5E5;
	}	.ead-questions--row-nobroder{

		background-color: #FFFFFF;
	}
	.ead-questions--title{
		font-size: 1.3em;
		color: #525252;
		padding: 14px;

	}
	.ead-questions--title p {

		display: inline;
	}
	.btn-questions{
		background-color: #2C7EDC;
		float: right;
		margin-top: -2px;
		border-radius: 26px;
		outline: none;
		padding: 5px 16px;
		border-color: transparent;
		transition: all 0.5s;
		-ms-transition: all 0.5s;
		-o-transition: all 0.5s;
		-webkit-transition: all 0.5s;
		outline: none


	}
	.btn-questions:hover{
		color: #fff;
		background-color: #66acfd;
		border-color: #ffffff;
		outline: none


	}
	.btn-questions:active,.btn-questions:focus{
		color: #fff;

		background-color: #2e64a5;
		border-color: #ffffff;
		outline: none

	}
	.ead-questions--form{

		height: 84.1vh;
		padding: 10px 0px
	}
	.ead-questions--title.title-2{
		color: #000;
		font-size: 1.2em;
		font-weight: 600;

		margin-bottom: 30px;
	}
	.ead-questions--form--group{
		border-bottom: 1px solid #E5E5E5;
		margin-bottom: 30px;

		height: 48px;
	}
	.ead-questions--form--group.textarea{
		height: 208px;

	}

	.ead-questions--form--group p{

		font-size: 1.2em;
	}
	.ead-formcontrol-input{

		width: 95%;
		border: none;
		padding: 0px 10px;
		font-size: 1em;
		text-align: center;
		font-size: 1.2em;
		border-left: 2px solid #9bcaff;
		border-right: 2px solid #9bcaff;
		outline: none;
		transition: all 0.5s;
		-ms-transition: all 0.5s;
		-o-transition: all 0.5s;
		-webkit-transition: all 0.5s;

	}
	.ead-formcontrol-input:focus{

		border-left: 2px solid #50C14E
		;
		border-right: 2px solid #50C14E
		;
	}

	.ead-formcontrol-input.textarea{
		margin: 0px;
		height: 184px;
		width: 100%;
		resize: none;
	}
	.ead-teacher--select{

		width: 65%;
		overflow: hidden;
		margin-left: 15px;
		float: inherit;
	}
	.ead-teacher--select ul{

		list-style: none;
		padding: 0px;
		width: 223%;
		margin: 0;
		padding: 0;
		position: relative;

	}
	.ead-teacher--select ul li{
		float: left;
		text-align: center;
		position: relative;
		width: 50%;
	}
	.ead-buttons--teacher{
		float: right;
		margin-right: 12px;
		margin-top: 2px;
	}
	.ead-button-teacher--previous,.ead-button-teacher--next{
		font-size: 1.7em;
		color: #3386d6;
		transition: all 0.5s;
		-ms-transition: all 0.5s;
		-o-transition: all 0.5s;
		-webkit-transition: all 0.5s;
		cursor:pointer;
	}.ead-button-teacher--previous:hover,.ead-button-teacher--next:hover{
		font-size: 1.7em;
		color: #66acfd;

	}
	.ead-btn--disabled{
		cursor: no-drop;
		color: #cacaca
	}

	.ead-btn--disabled:hover{
		cursor: no-drop;
		color: #cacaca
	}
	.ead-table--questions{
		height: 91vh;
		background-color: white;
		background-color: #FFFFFF;
		/* border-left: 1px solid #E5E5E5; */
		border-top: 1px solid #E5E5E5;
		padding: 30px;
	}
	.ead-table--questions .title-nav{
		font-size: 1.3em;
		padding: 0px 18px;
	}
	.ead-table--questions .title-nav a{
		text-decoration: none;

		color: #636363;

		margin-right: 36px;

		transition: all 0.2s;
		-ms-transition: all 0.2s;
		-o-transition: all 0.2s;
		-webkit-transition: all 0.2s;

	}
	.ead-table--questions .title-nav a span{
		transition: all 0.2s;
		-ms-transition: all 0.2s;
		-o-transition: all 0.2s;
		-webkit-transition: all 0.2s;
	}
	.title-nav.questionsAnswered .questionsAnswered,
	.title-nav.questionsConducted .questionsConducted{

		color: #337ab7;
	}
	.title-nav.questionsAnswered .questionsAnswered span,
	.title-nav.questionsConducted .questionsConducted span{
		border-bottom: 4px solid #438BDE;
		padding-bottom: 5px;

	}
	.ead-table{
		margin-top: 40px;
		border-top: 20px solid transparent;

		border-collapse: collapse;
	}
	.ead-table tbody{

		border-top: 20px solid transparent; 
	}
	.ead-table thead tr th{

		padding: 8px 16px;
		color: #2a2b2d;
		font-size: 1.1em;
		font-weight: 500;

	}
	.ead-table thead span{
		color: #337ab7;


	}

	.ead-table tbody tr{
		background-color: #F6F7FA;
		border-radius: 10px;  

		border-top: 8px solid white;

		cursor: pointer;
		transition: all 0.4s;
		-ms-transition: all 0.4s;
		-o-transition: all 0.4s;
		-webkit-transition: all 0.4s;
	}
	.ead-table tbody tr:hover{
		background-color: rgba(156, 202, 255, 0.41);  

	}
	.ead-table tbody tr td{
		padding: 8px 15px;  
	}
	.ead-table.loading:after,.ead-row-conteiner--messageView.loading:after {
		position: absolute;
		content: '';
		top: 44%;
		left: 50%;
		margin: -1.5em 0 0 -1.5em;
		width: 2em;
		height: 2em;
		-webkit-animation: segment-spin .6s linear;
		animation: segment-spin .6s linear;
		-webkit-animation-iteration-count: infinite;
		animation-iteration-count: infinite;
		border-radius: 500rem;
		border-color: #5094e1 rgba(0,0,0,.1) rgba(0,0,0,.1);
		border-style: solid;
		border-width: 0.4em;
		box-shadow: 0 0 0 1px transparent;
		visibility: visible;
		z-index: 101;
	}

	.ead-table.loading:before,.ead-row-conteiner--messageView.loading:before {
		position: absolute;
		content: '';
		top: 0;
		left: 0;
		background: rgba(255,255,255,.8);
		width: 100%;
		height: 100%;
		border-radius: .28571429rem;
		z-index: 100;
	}

	@-webkit-keyframes segment-spin {
		from {
			-webkit-transform: rotate(0);
			transform: rotate(0)
		}
		to {
			-webkit-transform: rotate(360deg);
			transform: rotate(360deg)
		}
	}

	@keyframes segment-spin {
		from {
			-webkit-transform: rotate(0);
			transform: rotate(0)
		}
		to {
			-webkit-transform: rotate(360deg);
			transform: rotate(360deg)
		}
	}

	.ead-text-danger{
		background-color: #FBB941;
		color: white;
		padding: 2px 0px;
		border-radius: 5px;
		font-size: 0.8em;
		font-weight: bold;

	}
	.ead-text-sucess{
		background-color: #4DB94A;
		color: white;
		padding: 2px 0px;
		border-radius: 5px;
		font-size: 0.8em;
		font-weight: bold;
	}
	.ead-table tbody td.status{
		width: 112px;
	}

	.ead-table thead th.view {

		padding: 8px 0px;
		text-align: center;
	}
	.ead-formcontrol--question{
		border: none;
		padding: 0px 10px;
		font-size: 1em;
		text-align: center;
		font-size: 1.2em;
		/* border-left: 2px solid #9bcaff; */
		/* border-right: 2px solid #9bcaff; */
		outline: none;
		transition: all 0.5s;
		-ms-transition: all 0.5s;
		-o-transition: all 0.5s;
		-webkit-transition: all 0.5s;
	}.ead-text-success--question{
		width: 52%;
		margin: 0px 24%;

		font-size: 1em!important;
	}
	.ead-formcontrol--question.area{
		max-height: 245px;
		overflow-y: auto;
		text-indent: 20px;

	}
	.ead-questions--form--group.area {
		height: 200px;
		max-height: 250px;
	}
	.pagination {

		margin: 0 0;
	}
	.btn-questions--lg{
		 margin-bottom: 25px;
    width: 100%;
    border-radius: 8px;

	}
	.ead-select-absolut{
		position: absolute;
	}
</style>

<header>
	<div class="ead-nav">
		<ol class="ead-breadcrumb">
			<li class="ead-breadcrumb-item"><a href="#">Aliança de Minas  </a></li>
			<li class="ead-breadcrumb-item"><a href="{{route('admin.panel')}}">Curso De Gestão Cultural</a></li>
			<li class="ead-breadcrumb-item active">Mensagens</li>

		</ol>
	</div>

</header>
<div id="table_content">
	<div class="ead-row--ajust--no">
		<div class="ead-row-conteiner--messageView">
			<div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 ead-plr--0 ead-row-conteiner--messageView" :class="{ 'loading': questions.statusLoading }" >

				<form action="{{route('admin.updatemessage', [])}}" method="post" enctype="multipart/form-data" > 
					<div class="ead-questions--row col-xs-12 col-sm-12 col-md-12 col-lg-12">
						{{ csrf_field()}}
						<div class="ead-questions--title">
						<p>@{{questions.response ? 'Responder pergunta:' : 'Selecione uma pergunta ao lado.' }}</p>
						</div>
					</div>

					<div class="ead-questions--row col-xs-12 col-sm-12 col-md-12 col-lg-12" >
						<div v-for="question in questions.active" class="ead-questions--row-noborder col-xs-12 col-sm-12 col-md-12 col-lg-12">

						<input class="hidden" type="text" name="id" value='@{{question.id}}'>



							<div class="ead-questions--form">
								<div class="ead-questions--form--group col-xs-12 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ead-plr--0">
										<p>Tema</p>
									</div>

									<div class="col-xs-4 col-sm-8 col-md-8 col-lg-8 ead-plr--0">
										<p class="ead-formcontrol--question">@{{question.title}}</p>
									</div>
								</div>

								<div class="ead-questions--form--group col-xs-12 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ead-plr--0">
										<p>Professor</p>
									</div>


									<div class="col-xs-4 col-sm-8 col-md-8 col-lg-8 ead-plr--0">
										<p class="ead-formcontrol--question">@{{question.name}}</p>
									</div>
								</div>
								<div class="ead-questions--form--group col-xs-12 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ead-plr--0">
										<p>Aluno</p>
									</div>


									<div class="col-xs-4 col-sm-8 col-md-8 col-lg-8 ead-plr--0">
										<p class="ead-formcontrol--question">@{{question.student}}</p>
									</div>
								</div>
								<div class="ead-questions--form--group col-xs-12 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ead-plr--0">
										<p>Status</p>
									</div>


									<div class="col-xs-4 col-sm-8 col-md-8 col-lg-8 ead-plr--0">
										<p class="ead-formcontrol--question ead-text-success--question" :class="{'ead-text-sucess': question.response_true, 'ead-text-danger': !questions.response_true}">@{{question.response_true ? 'Respondido' : 'Aguardando'}}</p>
									</div>
								</div>

								<div class="ead-questions--form--group area col-xs-12 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ead-plr--0">
										<p>Dúvida</p>
									</div>


									<div class="col-xs-4 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
										<p class="ead-formcontrol--question area">@{{question.description}}</p>
									</div>
								</div>

								<div class="ead-questions--form--group area col-xs-12 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ead-plr--0">
										<p>Resposta</p>
									</div>

									<div class="col-xs-4 col-sm-12 col-md-12 col-lg-12 ead-plr--0">
											<textarea  name="response" class="ead-formcontrol-input textarea" required>@{{question.response}}</textarea>
									</div>

								</div>
									<button type="submit" class="btn btn-lg btn-info btn-questions btn-questions--lg">enviar</button>


							</div>
						</div>
		</div>
					</form>
				
				</div>

			</div>
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 ead-plr--0">
				<div class="ead-table--questions">
					<div  class="table_content">

						<div class="title-nav" v-bind:class="tabActive">
							<a href="#Perguntas_Realizadas" class="questionsConducted" v-on:click="makeActive('questionsConducted')"><span> Perguntas Realizas</span></a>
							<a href="#Perguntas_Realizadas" class="questionsAnswered" v-on:click="makeActive('questionsAnswered')"><span> Perguntas Respondidas</span></a></div>

							<table class="table table-hover ead-table" :class="{ 'loading': loadingTable}">
								<thead>
									<tr>
										<th >

											<a href="" @click.prevent="doSort('title')">Tema</a>

											<span :class="{'ead-icon-sort-alpha-asc': interaction.sortColumn == 'title' && interaction.sortInverse == -1,
											'ead-icon-sort-alpha-desc' : interaction.sortColumn == 'title' && interaction.sortInverse == 1 }"></span>

										</th>


										<th >
											<a href="" @click.prevent="doSort('description')">Dúvida</a>


											<span :class="{'ead-icon-sort-alpha-asc': interaction.sortColumn == 'description' && interaction.sortInverse == -1,
											'ead-icon-sort-alpha-desc' : interaction.sortColumn == 'description' && interaction.sortInverse == 1 }"></span>


										</th>
										<th >

											<a href="" @click.prevent="doSort('student')">Aluno</a>

											<span class="ead-icon-sort-alpha-asc" :class="{'ead-icon-sort-alpha-asc': interaction.sortColumn == 'name' && interaction.sortInverse == -1,
											'ead-icon-sort-alpha-desc' : interaction.sortColumn == 'name' && interaction.sortInverse == 1 }"></span></th>

											<th >

												<a href="" @click.prevent="doSort('response_true')">Status</a>
												<span :class="{'ead-icon-sort-alpha-asc': interaction.sortColumn == 'response_true' && interaction.sortInverse == -1,
												'ead-icon-sort-alpha-desc' : interaction.sortColumn == 'response_true' && interaction.sortInverse == 1 }"></span>


											</th>




											<th width="1%" class="view"><a href="">Ver</a></th>

										</tr>
									</thead>
									<tbody v-for="question in message.list  | orderBy interaction.sortColumn interaction.sortInverse">
										<tr @click.prevent="goToUrl($index)">
											<td >@{{question.title}}</td>
											<td >@{{question.description | split}}</td>
											<td >@{{question.student}}</td>
											<td class="text-center status" > <p :class="{'ead-text-sucess': question.response_true, 'ead-text-danger': !question.response_true}">@{{question.response_true ? 'Respondido' : 'Aguardando'}}</p>
											</td>

											<td>

												<a href="mensagens/@{{question.id}}"><span class="ead-icon-eye"></span></a>

											</td>


										</tr>

									</tbody>
								</table>
								<nav class="text-center">
									<ul class="pagination">
										<li :class="{'disabled': pagination.currentPage ==1}">
											<a href="#" @click.prevent="previous" aria-label="Previous">
												<span aria-hidden="true">&laquo;</span>
											</a>
										</li>
										<li v-for='n in pagination.pageNumbers' :class="{'active': n == pagination.currentPage}"><a href="#" @click.prevent='page(n)'>@{{n}}</a></li>

										<li :class="{'disabled': pagination.currentPage == pagination.totalPages}">
											<a href="#" @click.prevent="next" aria-label="Next">
												<span aria-hidden="true">&raquo;</span>
											</a>
										</li>
									</ul>
								</nav>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
        <!--    <th>Título</th>

                <th>Descrição</th>

                <th>Professor</th>

                <th>Status</th>

                <th>Ver</th> -->
                @push('scripts')


                <script src="{{ asset('/vendor/js/TweenMax.min.js') }}"></script>
                <script src="{{ asset('/vendor/js/lodash.js') }}"></script>
<!--                 <script src="{{ asset('/vendor/js/vendor/vue/vue.min.js') }}"></script>
-->                <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
<script src="{{ asset('/vendor/js/vendor/vue/vue-resource.min.js') }}"></script>

<script type="text/javascript">
	$(function () {


		Vue.filter('split', function (value) {
			if(value.length>50){
				return value.substr(0,50)+('...');
			}
			
			return value;
		})


		new Vue({
			el: '#table_content'
			, data: {
				message: {
					all: []
					, list: []
					, paginated: []
				}
				, pagination: {

					currentPage: 1
					, perPage: 7
					, totalPages: 0
					, totalItems: 0
					, pageNumbers: []

				}
				, interaction: {
					visibleColumn: ['name']
					, filterTerm: ''
					, columnsToFilter: ['response_true']
					, openDetails: []
					, sortColumn: 'name'
					, sortInverse: 1
				}
				,
				loadingTable: false,
				tabActive: 'questionsConducted',
				questions:{
					status: false
					,active: []
					,statusLoading:false
					,response: false

				}


			}
			, ready: function () {

				var self = this;
				this.loadAnimation('loadingTable');
				$.get('/admin/mensagenslista', function (response) {

					self.$set('message.all', response);
					self.setPaginationOnData(response);

				});
				self.finishAnimation('loadingTable');

			}
			, methods: {

				setPaginationOnData: function (list) {
					this.loadAnimation('loadingTable');

					self = this;

					chunk = _.chunk(list, self.pagination.perPage);

					self.$set('message.paginated', chunk);

					self.$set('message.list', chunk[0]);

					self.$set('pagination.totalItems', list.length);
					self.$set('pagination.totalPages', Math.ceil(list.length / self.pagination.perPage));
					self.$set('pagination.pageNumbers', _.range(1, self.pagination.totalPages + 1));
					this.finishAnimation('loadingTable');

				}
				, next: function () {
					self = this;

					if (self.pagination.currentPage >= self.pagination.totalPages) {
						return false;
					}

					this.loadAnimation('loadingTable');

					self.$set('pagination.currentPage', self.pagination.currentPage + 1);
					self.$set('message.list', self.message.paginated[self.pagination.currentPage - 1]);
					this.finishAnimation('loadingTable');
				}
				, previous: function () {
					self = this;

					if (self.pagination.currentPage == 1) {

						return false;
					}
					this.loadAnimation('loadingTable');

					self.$set('pagination.currentPage', self.pagination.currentPage - 1);
					self.$set('message.list', self.message.paginated[self.pagination.currentPage - 1]);
					this.finishAnimation('loadingTable');

				},
				loadAnimation: function (element) {
					this.$set(element,true);
				},
				finishAnimation: function (element) {
					self = this;
					setTimeout(function () {
						self.$set(element,false);
					},200);

				}
				, page: function (page) {
					this.loadAnimation('loadingTable');

					self = this;

					self.$set('pagination.currentPage', page);
					self.$set('message.list', self.message.paginated[page - 1]);
					this.finishAnimation('loadingTable');

				},
				setDetails: function (id) {
					var self = this;

					index = self.interaction.openDetails.indexOf(id);

					if (index > -1) {

						self.interaction.openDetails.$remove(id);

					} else {

						self.interaction.openDetails.push(id);

					}
				}
				, doReset: function () {
					self = this;
					self.$set('interaction.visibleColumn', ['name'])
					self.$set('interaction.filterTerm', '');
					self.$set('interaction.columnsToFilter', ['response_true']);
					self.$set('interaction.openDetails', []);
					self.$set('interaction.sortColumn', 'name');
					self.$set('interaction.sortInverse', 1);
					self.$set('pagination.currentPage', 1);

					self.setPaginationOnData(self.message.all)

				}
				, doSort: function (column) {
					this.loadAnimation('loadingTable');

					var self = this;

					self.interaction.sortColumn = column;
					self.interaction.sortInverse = self.interaction.sortInverse * -1
					this.finishAnimation('loadingTable');

				}
				, doFilter() {

					self = this;
					filtered = self.message.all;

					if (self.interaction.columnsToFilter.length > 0) {

						filtered = _.filter(self.message.all, function (question) {
							return self.interaction.columnsToFilter.some(function (column) {
								return question[column] == self.interaction.filterTerm
//.toLowerCase().indexOf(self.interaction.filterTerm.toLowerCase()) > -1
})
						})


						self.$set('pagination.currentPage', 1);
						self.setPaginationOnData(filtered)



					}

				},
				goToUrl: function (id) {
					self = this;
					self.loadAnimation('questions.statusLoading');
					self.$set('questions.status',true);
					data = self.message.list[id];
					self.questions.active.splice(0);
					self.questions.active.push(data);
					self.questions.response = data['response_true'];
					setTimeout(function () {
						self.finishAnimation('questions.statusLoading');

					},200);

				},
				makeActive: function(item){
					self = this;
					this.loadAnimation('loadingTable');

					this.tabActive = item;
					if(item == 'questionsAnswered'){
						self.interaction.filterTerm = true
						self.doFilter();
					}else{
						self.doReset();
					}
					this.finishAnimation('loadingTable');

				},
				backQuestion: function () {
					self = this;
					self.loadAnimation('questions.statusLoading');
					self.$set('questions.status',false);
					setTimeout(function () {
						self.finishAnimation('questions.statusLoading');

					},200);

				}

			}
		});


});



</script> 

@endpush

@endsection
