@extends('Admin.default')

@section('content')
	<link rel="stylesheet" href="{{asset('/vendor/css/plyr.css')}}"></link>
	<style type="text/css">
		.ead-course--steps{
			transition: all 0.5s;
			-webkit-transition: all 0.5s;
			-moz-transition: all 0.5s;
			-ms-transition: all 0.5s;
			-o-transition: all 0.5s;

		}
		.ead-course--steps.active{	
    		border: 2px solid #59dc7b;
		}
		.ead-course--steps.active .ead-course--position span {
    background-color: #59dc7b;

		}
		legend{
			    position: absolute;
    top: 68%;
    text-align: center;
    background-color: white;
    font-size: 2em;
    text-transform: uppercase;
    		}
	</style>

	<header>
		<div class="ead-nav">
			<ol class="ead-breadcrumb">
				<li class="ead-breadcrumb-item"><a href="#">Aliança de Minas	</a></li>
				<li class="ead-breadcrumb-item"><a href="{{route('admin.panel')}}">Curso De Gestão Cultural</a></li>
				<li class="ead-breadcrumb-item active">Aulas</li>

			</ol>
		</div>

	</header> 

		<div class="ead-row--ajust">
			<div class="ead-container--data ">
				<div class=" col-md-8">
					<div class="ead-preview ead-preview--16p9">
						<figure>
							@if(!$erros)

							 <img src="{{ asset('vendor/img/videos/bg-video.jpg')}}">
							 <legend>Este Vídeo não está disponível</legend>

							 @else

							 <video width="100%" height="100%" >
							 <source src="{{ route('getVideo',$nameVid)  }}" type="video/mp4">
							 </video>
							 @endif
						
						</figure>
					</div>
					<a href="{{route('admin.panel')}}">
						<div class="ead-course--steps {{ $activeVideo == 1 ? 'active' : '' }}">
							<div class="ead-course--position col-md-2">
								<span>1</span>
							</div>
							<div class="ead-course--information col-md-8 text-center">
								<h3>Módulo Educação Patrimonial</h3>
							</div>
							<div class="col-md-2">
								<div class="ead-course--icon text-center">


									<span class="ead-course-icon--font  ead-icon-stopwatch"></span>
									<span class="ead-course-icon--text">2 Horas</span>
								</div>
							</div>
						</div>
					</a>
				
					<a href="{{route('admin.panelVideo','Modulo-Politica-Cultural')}}">

						<div class="ead-course--steps {{ $activeVideo == 2 ? 'active' : '' }}">
							<div class="ead-course--position col-md-2">
								<span>2</span>
							</div>
							<div class="ead-course--information col-md-8 text-center">
								<h3>Módulo Política Cultural</h3>
							</div>
							<div class="col-md-2">
								<div class="ead-course--icon text-center">


									<span class="ead-course-icon--font  ead-icon-stopwatch"></span>
									<span class="ead-course-icon--text">2 Horas</span>
								</div>
							</div>
						</div>
					</a>

				</div>

				<div class="col-md-4">
					<div class="ead-information col-md-12 ead-plr--0">
						<div class="ead-information--header">
							<h3 class="ead-information--title text-center">Informações</h3>
						</div>
						<div class="ead-information--content col-md-12">
							<p >&nbsp;A Aliança de Minas - Soluções em Meio Ambiente e Cultura, junto a Ampla - Assistência aos Municípios, tem o prazer de apresentar o curso de Gestão Cultural.
								<br><br>
								&nbsp;O Curso tem como objetivo a capacitação do aluno e o aprimoramento dos gestores do patrimônio cultural público e privado, além de estimular o interesse dos futuros gestores. 
								<br><br>&nbsp;O Curso propõe neste módulo I duas aulas de duas horas - 120 minutos cada.</p>
							</div>
						</div>

					</div>

				</div>

			</div>

	@push('scripts')

		<script type="text/javascript" src="{{asset('/vendor/js/plyr.js')}}"></script>

<script>

if($('video').length > 0){

	plyr.setup();
}

</script>

	@endpush

@endsection