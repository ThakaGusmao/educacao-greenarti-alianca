@extends('Admin.default')
@section('content')
<style type="text/css">

body::-webkit-scrollbar
{
	width: 8px;
	background-color: transparent;
}

body::-webkit-scrollbar-thumb
{
	background-color: #888;
}

	.ead-file--files{
		width: 100%;
		margin-bottom: 20px;
		line-height: 80px;
		border-radius: 5px;
		background-color: white;
		/* padding: 30px; */
		text-align: center;
		display: table;
		height: 80px;

		border: 2px solid #e8e8e8;
	}

	.ead-file--type>.ead-file-icon--font{
		/* background-color: #5093E1; */
		/* height: 33px; */
		padding: 1px 24px;
		/* border-radius: 100%; */
		color: #5093E1;
		/* font-weight: bold; */
	}

	.ead-file--type{
		padding: 0px!important;
		line-height: 0px;
		height: 100%;
		height: 80px;
		padding: 0px;

		font-size: 19px;
		background-color: #f9f9f9;
		cursor: pointer;

	}

	.ead-file--information p{
		color: #3E87DD;
		margin-top: 0px;
		margin-bottom: 0px;

		vertical-align: middle;
		display: inline-block;
		line-height: 1.1;
		text-align: center;
	}
	.ead-file--information:after {
		color: #ccc;
		content: "";
		position: absolute;
		right: 0%;
		height: 100%;
		top: 0%;
		/* bottom: 0; */
		/* top: 9px; */
		/* background-color: aqua; */
		border-left: 2px solid #EFF1F6;
	}

	.ead-file--icon {
		line-height: 00px;
		height: 100%;
		height: 80px;
		padding: 0px;

	}
	.ead-file-icon--font{
		width: 100%;
		font-size: 1.8em;
		padding: 0px 15px;
		line-height: 60px;
		/* height: 80px; */
		color: #3e87dd;
	}
	.ead-file--information {
		cursor: pointer
	}
	.ead-file-icon--text{
		line-height: 0px;
		font-family: 'Open Sans', sans-serif;
		font-weight: bold;
		color: #818A97;
		font-size: 14px;

		display: block;

	}
	.ead-information{
		background-color: white;
		border-radius: 6px;

		cursor: pointer;

	}

	.ead-file-icon--text,.ead-file-icon--font{
		transition: 0.5s all;
		-ms-transition: 0.5s all;
		-o-transition: 0.5s all;
		-webkit-transition: 0.5s all;
	}

	.ead-file--download{
		outline: none;

	}
	.ead-file--download:hover{
		text-decoration: none;

	}
	.ead-file--download:hover .ead-file-icon--text,.ead-file--download:hover .ead-file-icon--font{
		color: #7BD28D;

	}
	.ead-file-list{
		padding: 0px
	}


	#ead-drop{
		background-color: #edeff5;
		padding: 40px 50px;
		margin: 30px;
		border: 4px dashed rgba(80, 147, 225, 0.59);
		/* border-radius: 3px; */
		text-align: center;
		text-transform: uppercase;
		font-size: 16px;
		font-weight: bold;
		color: #7f858a;
		font-family: 'Open Sans', sans-serif;

	}


	.ead-file--type canvas{
		width: 76px;
		height: 76px;
	}
	.btn-file {
		position: relative;
		overflow: hidden;
	}
	.btn-file input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		min-width: 100%;
		min-height: 100%;
		font-size: 100px;
		text-align: right;
		filter: alpha(opacity=0);
		opacity: 0;
		outline: none;
		background: white;
		cursor: inherit;
		display: block;
	}
	.ead-file-btn{
		color: #3e87dd;
		/* background-color: #fff; */
		border-color: #3e87dd;
		font-size: 1em;
		float: none;
		padding: 8px 33px;
		border: 2px solid;
		border-radius: 7px;
		font-family: 'Open Sans', sans-serif;
		transition: 0.5s all;
		-ms-transition: 0.5s all;
		-o-transition: 0.5s all;
		-webkit-transition: 0.5s all;

	}
	.ead-file-btn span{
		padding: 0px 12px 0px 0px;
	}
	.ead-file-btn:hover{
		color:#5ba6ff;
	}
	.ead-file--loading,.ead-file--state {
		width: 100%;
		height: 100%;
		position: absolute;
		background-color: rgba(237, 239, 245, 0.82)}
		.ead-file--loading input{
			display: none
		}

		.ead-file--state span {
			width: 15px;
			height: 12px;
			background: url('http://educacao.app/vendor/img/file/icons.png') no-repeat;
			position: absolute;
			top: 50%;
			left: 50%;
			cursor: pointer;
			transform: translate(-50%,-50%);
			-webkit-transform: translate(-50%,-50%);
			-ms-transform: translate(-50%,-50%);
			-o-transform: translate(-50%,-50%);
		}
		.ead-file--files.working .ead-file--state span {
			height: 16px;
			background-position: 0 -12px;
		}
		.ead-file--remove{

			border: 2px solid #F65177;
		}
		.ead-file--delete{
			position: fixed;
			bottom: 4%;
			right: 2%;
			width: 75px;
			height: auto;

			cursor: pointer;
		}
		.ead-file--tresh{fill:#2f323a;opacity: 0}
		.ead-file-tresh-stroke{fill:none;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
		.ead-tresh--bucket,.ead-tresh--cover{opacity: 0}
		.ead-modal{
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			overflow-y: auto;
			-webkit-perspective: 500px;
			perspective: 500px;
			-webkit-perspective-origin: center;
			perspective-origin: center;
			font-family: 'Open Sans', sans-serif;
			opacity: 0;


		}
		.ead-modal--box{
			background: white;
			border-radius: 4px;
			position: relative;
			outline: none;
			padding: 15px 15px 0;
			box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2);
			border-radius: 5px;
			height: 170px;	
			position: absolute;
			top: 0;
			margin: auto;
			bottom: 0;
			left: 0;
			right: 0;

		}
		.ead-modal-bg{
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			-webkit-transition: all .4s;
			transition: all .4s;
			background-color: rgba(0, 0, 0, 0.2);
			opacity: 0;

		}
		.ead-modal--container {
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			z-index: 99999999;
			font-family: inherit;
			overflow: hidden;
			opacity: 0;

		}
		.ead-modal--title{
			font-size: inherit;
			font-family: inherit;
			display: inline-block;
			vertical-align: middle;
			padding-bottom: 15px;
			font-size: 1.5em;
			font-weight: bolder;
		}
		.ead-modal--contet{
			margin-bottom: 15px;
			height: auto;
			-webkit-transition: height 0.4s ease-in;
			transition: height 0.4s ease-in;
			display: inline-block;
			width: 100%;
			position: relative;
			font-size: 1.2em;
		}
		.ead-modal--butons{
			float: right;
		}
	</style>
	<header>
		<div class="ead-nav">
			<ol class="ead-breadcrumb">
				<li class="ead-breadcrumb-item"><a href="#">Aliança de Minas	</a></li>
				<li class="ead-breadcrumb-item"><a href="{{route('admin.panel')}}">Curso De Gestão Cultural</a></li>
				<li class="ead-breadcrumb-item active">Arquivos</li>

			</ol>
		</div>

	</header> 
	<div class="ead-row--ajust">

		<form id="upload" action="{{route('admin.addentry', [])}}" method="post" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div id="ead-drop">
				<h3>Arraste os arquivos.</h3>
				<label class="btn ead-file-btn btn-file">
					<span class="ead-icon-upload3"></span> Selecionar	<input type="file" name="upl" multiple />

				</label>
			</div>

		</form>

		<ul class="ead-file-list">

			@foreach($entries as $entry)

			<div class="col-xs-6 col-md-3">
				<div class="ead-file--files">
					<div class="ead-file--type col-md-3">
						<span class="hide id">{{$entry->id}}</span>
						<span class="ead-file-icon--font ead-icon-file-text2 ead-icon-file-{{$entry->extension}}"></span>
						<span class="ead-file-icon--text">{{$entry->size}} MB</span>

					</div>
					<div class="ead-file--information col-md-6 text-center">
						<p>{{$entry->original_filename}}</p>
					</div>

					<div class="col-md-3 ead-file--icon text-center">
						<a class="ead-file--download" href="get/{{$entry->filename}}" download='{{$entry->original_filename}}'>
							<span class="ead-file-icon--font ead-icon-download3"></span>
							<span class="ead-file-icon--text">Baixar</span></a>

						</div>
					</div>
				</div>	


				@endforeach
			</ul>

			<div class="ead-file--delete">
				<svg class="ead-file-treash" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				viewBox="0 0 117.7 117.7" style="enable-background:new 0 0 117.7 117.7;" xml:space="preserve">
				<g id="ead-tresh--circle">
					<circle class="ead-file--tresh" cx="58.8" cy="58.8" r="58.8"/>
				</g>
				<g id="ead-tresh--bucket">
					<path class="ead-file-tresh-stroke" d="M38.8,50.2V80c0,3.8,3.1,6.8,6.8,6.8H72c3.8,0,6.8-3.1,6.8-6.8V50.2"/>
					<g>
						<line class="ead-file-tresh-stroke" x1="52" y1="54.2" x2="52" y2="74.8"/>
						<line class="ead-file-tresh-stroke" x1="65.7" y1="54.2" x2="65.7" y2="74.8"/>
					</g>
				</g>
				<g id="ead-tresh--cover">
					<line class="ead-file-tresh-stroke" x1="38.8" y1="37.2" x2="78.8" y2="37.2"/>
					<path class="ead-file-tresh-stroke" d="M52,37.2v-5.5c0-3.8,3.1-6.8,6.8-6.8h0c3.8,0,6.8,3.1,6.8,6.8v5.5"/>
				</g>
			</svg>
		</div>
	</div>
	<div class="ead-modal--container">
		
		<div class="ead-modal-bg"></div>

		<div class="ead-modal">
			<div class=" ead-modal--box col-md-3 col-md-offset-3 col-sm-5 col-sm-offset-4 col-xs-10 col-xs-offset-1">

				<div class="ead-modal--title">
					<span>Confirmação</span>
				</div>
				<div class="ead-modal--contet">
					Você realmente deseja excluir os arquivos selecionados ?

				</div>
				<div class="ead-modal--butons">
					<button class="btn btn-info ead-modal--confirm">Continuar</button>
					<button class="btn btn-default ead-modal--close">Fechar</button>
				</div>

				
			</div>
		</div>

	</div>

	

	@push('scripts')

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="{{ asset('/vendor/js/FileUpload/jquery.knob.js') }}"></script>

	<script src="{{ asset('/vendor/js/FileUpload/jquery.ui.widget.js') }}"></script>
	<script src="{{ asset('/vendor/js/FileUpload/jquery.iframe-transport.js') }}"></script>
	<script src="{{ asset('/vendor/js/FileUpload/jquery.fileupload.js') }}"></script>
	<script src="{{ asset('/vendor/js/TweenMax.min.js') }}"></script>




	<script type="text/javascript">
		$(function(){
			var	$contentFile = $('.ead-file--type, .ead-file--information'),
			$FilesDelete = $('.ead-file--delete'),
			$secure_token = $('#upload > input[type="hidden"]').val(),
			$treashAnimate = $('.ead-file-treash'),
			$treshCircle = $treashAnimate.find('circle'),
			$treshCover = $treashAnimate.find('#ead-tresh--bucket'),
			$treshBucket = $treashAnimate.find('#ead-tresh--cover');
			$modal = $('.ead-modal');
			$modalBg = $('.ead-modal-bg');
			$modalContainer = $('.ead-modal--container');

			$('#upload').fileupload({
				dropZone: $('#ead-drop'),
				add: function (e, data) {
					var tpl = $("<div class='col-xs-6 col-md-3'><div class='ead-file--files working'><div class='ead-file--type col-md-3'><div class='ead-file--loading'><input></input></div><span class='ead-file-icon--font ead-icon-file-text2'></span><span class='ead-file-icon--text ead-file-text--size'></span></div><div class='ead-file--information col-md-6 text-center'><p></p></div><div class='col-md-3 ead-file--icon text-center'><div class='ead-file--state'><span></span></div><a class='ead-file--download' href='' download><span class='ead-file-icon--font ead-icon-download3'></span><span class='ead-file-icon--text'>Baixar</span></a></div></div></div></div>");

					tpl.find('p').text(data.files[0].name);
					tpl.find('.ead-file-text--size').text(formatFileSize(data.files[0].size));

					data.context = tpl.appendTo($('.ead-file-list'));

					tpl.find('.ead-file--loading input').knob({
						'change' : function (v) {  return false; }

					});

					tpl.find('.ead-file--state span').click(function(){
						if(tpl.hasClass('working')){
							jqXHR.abort();
							tpl.fadeOut(function(){
								tpl.remove();
							});
						}

					});

					var jqXHR = data.submit();
				},
				progress: function(e, data){

					var progress = parseInt(data.loaded / data.total * 100, 10);

					data.context.find('input').val(progress).change();

					if(progress == 100){
						data.context.find('.ead-file--files').removeClass('working');
						setTimeout(function () {
							data.context.find('.ead-file--loading').fadeOut(300);
								//data.context.find('.ead-file--state').fadeOut(300);

							}, 400)

					}

				}
			});

			function formatFileSize(bytes) {
				if (typeof bytes !== 'number') {
					return '';
				}

				if (bytes >= 1000000000) {
					return (bytes / 1000000000).toFixed(2) + ' GB';
				}

				if (bytes >= 1000000) {
					return (bytes / 1000000).toFixed(2) + ' MB';
				}
				return (bytes / 1000).toFixed(2) + ' KB';
			}


			$contentFile.each(function(index, el) {

				$(this).click(function(event) {

					event.preventDefault();

					if($(this).parent().hasClass('ead-file--remove')){

						$(this).parent().removeClass('ead-file--remove');

					}else{
						$(this).parent().addClass('ead-file--remove');

						if($('.ead-file--remove').length==1){
							$tl__treash.play();
						}else{
							
							shakeTreash();

						}
					}
					if($('.ead-file--remove').length <=0){
						$tl__treash.reverse();

					}
				});


			});

			var $tl__treash = new TimelineMax();
			var $tl__modal = new TimelineMax();

			$tl__treash.set($treshCircle,{
				autoAlpha:0,
				scale:0,
				transformOrigin:'50% 50%',
				overwrite: "all",
			}).set($treshCover,{
				autoAlpha:0,
				rotation: -50,
				transformOrigin:'50% 50%',
				overwrite: "all",
			}).set($treshBucket,{
				autoAlpha:0,
				rotation: 50,
				transformOrigin:'50% 50%',
				overwrite: "all",
			}).pause().to($treshCircle,0.6,{
				scale:1,
				autoAlpha:1,
				ease: Back.easeOut.config(0.5),
				overwrite: "all",
			}).to($treshBucket,0.5,{
				scale:1,
				autoAlpha:1,
				rotation: 0,
				ease: Back.easeOut.config(1.9),
				overwrite: "all",
			},"-=0.6").to($treshCover,0.5,{
				scale:1,
				autoAlpha:1,
				rotation: 0,
				ease: Back.easeOut.config(1.9),
				overwrite: "all",
			},"-=0.5");


			$tl__modal.set([$modalContainer,$modalBg],{
				autoAlpha:0
			}).set($modal,{
				autoAlpha:0,
				scale:0,
				transformOrigin: '50% 50%'
			}).pause().to([$modalContainer,$modalBg],0.5,{
				autoAlpha:1
			}).to($modal,0.6,{
				scale:1,
				autoAlpha:1,
				ease: Back.easeOut.config(1)
			},"-=0.5")



			$FilesDelete.click(function(event) {
				event.preventDefault();
				
				$tl__modal.play();

				$('.ead-modal--confirm').click(function(event) {
					event.preventDefault();
					$tl__modal.reverse();
					startDelete();

				});
				$('.ead-modal--close').click(function(event) {
					event.preventDefault();
					$tl__modal.reverse();

				});

			});
			function startDelete(){
				$files = $('.ead-file--remove');

				var $data = new Array();

				if($files.length>0){
					$files.each(function(index, el) {
						var obj= new Object();
						obj.id = $(this).find('.id').text();
						$data.push(obj);
					});

						$dataPost= JSON.stringify($data);
						
						$.ajax({
							url: '/admin/delete',
							headers: { 'X-CSRF-TOKEN': $secure_token},
							type: 'POST',
							dataType: 'json',
							data: $dataPost
						})	
						.done(function() {
							TweenMax.staggerTo($files.parent(),0.6,{
								scale:0,
								autoAlpha:0,
								ease: Back.easeIn,
								onComplete: deleteFiles
							},0.2);


							$tl__treash.reverse();


						})
						

					}
				}

				$treashAnimate.hover(function() {

					TweenMax.to($treshBucket, 0.2,{rotation:20});
					TweenMax.to($treshCircle, 0.2,{fill:'#4a5b6f'});

				}, function() {
					TweenMax.to($treshBucket, 0.2,{rotation:0});
					TweenMax.to($treshCircle, 0.2,{fill:'#2f323a'});


				});
				function shakeTreash() {
					TweenMax.to([$treshCover,$treshBucket], 0.2,{rotation:10});
					TweenMax.to([$treshCover,$treshBucket], 0.2,{rotation:-10, delay: .2});
					TweenMax.to([$treshCover,$treshBucket], 0.2,{rotation:0, delay: .4});

				}

				function deleteFiles(){
					$files.parent().remove();
				}

			})

		</script>

		@endpush

		@endsection
