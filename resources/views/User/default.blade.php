<!DOCTYPE html>
<html>
<head>
	<title>Educação - Curso Gestão Cultural</title>
</head>
<link rel="stylesheet" href="{{ asset('/vendor/css/vendor.css') }}"></link>
<link rel="stylesheet" href="{{ asset('/vendor/css/icons.css') }}"></link>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<style type="text/css">
body::-webkit-scrollbar
{
	width: 8px;
	background-color: transparent;
}

body::-webkit-scrollbar-thumb
{
	background-color: #888;
}
	body,html{
		margin: 0px;
		padding:0px;
		background-color: #EDEFF5;
		width: 100%;
		height: 100%;

		margin-bottom: 3em;
				font-family: 'Open Sans', sans-serif;

	}
	.ead-nav{
		background: white;
		height: 70px;
		font-size: 1.2em;
		font-family: 'Open Sans', sans-serif;
		padding: 14px 45px;
	}

	.ead-menu>ul{
		padding: 0px;
		text-align: center;

		margin-top: 130px;
	}
	.ead-menu>ul>li{
		width: 80px;
		height: 80px;

		display: table;
	}
	.ead-menu>ul>li>a{
		outline: none;
		text-decoration: none;
		font-size: 2.2em;
		color: #F2F7FD;
		display: table-cell;
		vertical-align: middle;
		transition: all 0.5s;
		-ms-transition: all 0.5s;
		-webkit-transition: all 0.5s;
		-o-transition: all 0.5s;

	}
	.ead-menu>ul>li>a.active{
    background-color: rgba(80, 147, 225, 0.17);
    -webkit-box-shadow: inset 5px 0px 0px 0px rgba(80,148,225,1);
    -moz-box-shadow: inset 5px 0px 0px 0px rgba(80,148,225,1);
    box-shadow: inset 5px 0px 0px 0px rgba(80,148,225,1);
	}
	body > nav > footer > ul > li{
				-ms-transition: all 0.5s;
		-webkit-transition: all 0.5s;
		-o-transition: all 0.5s;
		-moz-transition: all 0.5s;
	}
	.ead-menu>ul>li>a:hover,
	body > nav > footer > ul > li:hover{
    background-color: rgba(80, 147, 225, 0.17);
	}
	.ead-breadcrumb{
		padding: 8px 0px;
		list-style: none;
		border-radius: 4px;

	}
	.ead-breadcrumb-item{
		display: inline-block;
	}
	.ead-breadcrumb>.active {
		color: #777;
	}
	.ead-breadcrumb>li+li:before {
		padding: 0 5px;
		color: #ccc;
		content: "|\00a0";
	}
	.ead-preview{
		width: 100%;
		background: white;
		border-radius: 5px;
		margin-bottom: 60px;
	}
	.ead-preview--16p9{

		padding-top: 56.25%;
	}
	.ead-preview figure{
		width: 96%;
		position: absolute;
		top: 0%;
	}
	
	.ead-preview figure img{
		width: 100%;
		height: auto
	}

	.ead-container--data{
		margin-top: 60px;
	}
	.ead-plr--0{
		padding-right: 0px;
		padding-left: 0px;
	}
	.ead-course--steps{
		width: 60%;
		margin-bottom: 20px;
		line-height: 80px;
		border-radius: 5px;
		background-color: white;
		/* padding: 30px; */
		text-align: center;
		display: table;
		height: 80px;

		border: 2px solid #e8e8e8;
	}
	.ead-course--position span{
		background-color: #5093E1;
		/* height: 33px; */
		padding: 10px 14px;
		border-radius: 100%;
		color: white;
		font-weight: bold;
	}
	.ead-course--position{
		background-color: #f9f9f9
	}
	.ead-course--information h3{
		color: #3E87DD;
		margin-top: 0px;
		margin-bottom: 0px;

		vertical-align: middle;
		display: inline-block;
	}
	.ead-course--information:after {
		color: #ccc;
		content: "";
		position: absolute;
		right: 0%;
		height: 100%;
		/* bottom: 0; */
		/* top: 9px; */
		/* background-color: aqua; */
		border-left: 2px solid #EFF1F6;
	}

	.ead-course--icon {
		line-height: 00px;
		height: 100%;
		height: 80px;
	}
	.ead-course-icon--font{
		width: 100%;
		font-size: 1.8em;
		padding: 0px 20px;
		line-height: 60px;
		/* height: 80px; */
		color: #3e87dd;
	}
	.ead-course-icon--text{
		line-height: 0px;
		font-family: 'Open Sans', sans-serif;
		font-weight: bold;
		color: #818A97

	}
	.ead-information{
		background-color: white;
		border-radius: 6px;

	}
	.ead-information--header{
		height: 40px;
		border-bottom: 2px solid #EFF1F6;
		width: 100%;

	}
	.ead-information--title{
		font-family: 'Open Sans', sans-serif;
		text-transform: uppercase;
		color: #3E87DD;
	}

	.ead-information--content {
		padding: 30px
	}

	.ead-information--content p{
		color: #6d6d6d;
		font-size: 1.2em;
	}
	
	.ead-menu{
		width: 80px;
		height: 100%;
		position: fixed;
		top: 0%;
		left: 0%;
		background-color: #2F323A;
		
    z-index: 10000;

	}
	.ead-row{
		margin-left: 80px;
	}
	.ead-row--ajust{
		padding: 0px 30px;
	}

.ead-nav--footer{
		position: absolute;
    bottom: 0%;
    width: 100%;
    /* text-align: center; */
    font-size: 3.6em;
    text-align: center;
	}
	.ead-nav--footer ul {
		    list-style: none;
    padding: 0px;
	}
	.ead-nav--footer ul li a{
			text-decoration: none;
			    color: #F2F7FD;
	}
	.ead-nav--text{
		    float: left;
    line-height: 0.5;
    font-size: 0.4em;
    width: 100%;
	}

	.ead-logout{
		font-size: 0.8em;
		transition: all 0.5s;
		-ms-transition: all 0.5s;
		-moz-transition: all 0.5s;
		-o-transition: all 0.5s;
		-webkit-transition: all 0.5s;
	}
	.ead-logout:hover{

    color: #e4e9f0;
	}


</style>
<body>
	<nav class="ead-menu">
		<ul>
			<li>
				<a href="{{route('user.panel')}}" class="{{ Route::currentRouteName() == "user.panel" || "user.panelVideo" ? 'active' : '' }}"><span class="ead-icon-video"></span><span class="ead-nav--text">Aulas</span></a>
			</li>
			<li>
				<a href="{{route('user.files')}}" class="{{ Route::currentRouteName() == "user.files" ? 'active' : '' }}"><span class="ead-icon-folder"></span><span class="ead-nav--text">Arquivos</span></a>
			</li>
			<li>
				<a href="{{route('user.message')}}" class="{{ Route::currentRouteName() == "user.message" ? 'active' : '' }}"><span class="ead-icon-message"></span><span class="ead-nav--text">Mensagens</span></a>	
			</li>
		</ul>
					<footer class="ead-nav--footer">
			
			<ul><li>
				<a href="/logout"><span class="ead-logout ead-icon-sign-out"></span></a>
			</li></ul>

		</footer>

	</nav>
	<div class="ead-row">

	

		@yield('content')

	</div>




	<script type="text/javascript" src="{{ asset('/vendor/js/vendor.js') }}"></script>
		@stack('scripts')

</body>
</html>