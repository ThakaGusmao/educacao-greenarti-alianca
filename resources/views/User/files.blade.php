@extends('User.default')
@section('content')
<style type="text/css">
body::-webkit-scrollbar
{
	width: 8px;
	background-color: transparent;
}

body::-webkit-scrollbar-thumb
{
	background-color: #888;
}
	.ead-file--files{
		width: 100%;
		margin-bottom: 20px;
		line-height: 80px;
		border-radius: 5px;
		background-color: white;
		/* padding: 30px; */
		text-align: center;
		display: table;
		height: 80px;

		border: 2px solid #e8e8e8;
	}

	.ead-file--type>.ead-file-icon--font{
		/* background-color: #5093E1; */
		/* height: 33px; */
		padding: 1px 24px;
		/* border-radius: 100%; */
		color: #5093E1;
		/* font-weight: bold; */
	}

	.ead-file--type{
		padding: 0px!important;
		line-height: 0px;
		height: 100%;
		height: 80px;
		padding: 0px;

		font-size: 19px;
		background-color: #f9f9f9;
		cursor: pointer;

	}

	.ead-file--information p{
		color: #3E87DD;
		margin-top: 0px;
		margin-bottom: 0px;

		vertical-align: middle;
		display: inline-block;
		line-height: 1.1;
		text-align: center;
	}
	.ead-file--information:after {
		color: #ccc;
		content: "";
		position: absolute;
		right: 0%;
		height: 100%;
		top: 0%;
		/* bottom: 0; */
		/* top: 9px; */
		/* background-color: aqua; */
		border-left: 2px solid #EFF1F6;
	}

	.ead-file--icon {
		line-height: 00px;
		height: 100%;
		height: 80px;
		padding: 0px;

	}
	.ead-file-icon--font{
		width: 100%;
		font-size: 1.8em;
		padding: 0px 15px;
		line-height: 60px;
		/* height: 80px; */
		color: #3e87dd;
	}
	.ead-file--information {
		cursor: pointer
	}
	.ead-file-icon--text{
		line-height: 0px;
		font-family: 'Open Sans', sans-serif;
		font-weight: bold;
		color: #818A97;
		font-size: 14px;

		display: block;

	}
	.ead-information{
		background-color: white;
		border-radius: 6px;

		cursor: pointer;

	}

	.ead-file-icon--text,.ead-file-icon--font{
		transition: 0.5s all;
		-ms-transition: 0.5s all;
		-o-transition: 0.5s all;
		-webkit-transition: 0.5s all;
	}

	.ead-file--download{
		outline: none;

	}
	.ead-file--download:hover{
		text-decoration: none;

	}
	.ead-file--download:hover .ead-file-icon--text,.ead-file--download:hover .ead-file-icon--font{
		color: #7BD28D;

	}
	.ead-file-list{
		padding: 0px
	}


	#ead-drop{
		background-color: #edeff5;
		padding: 40px 50px;
		margin: 30px;
		border: 4px dashed rgba(80, 147, 225, 0.59);
		/* border-radius: 3px; */
		text-align: center;
		text-transform: uppercase;
		font-size: 16px;
		font-weight: bold;
		color: #7f858a;
		font-family: 'Open Sans', sans-serif;

	}


	.ead-file--type canvas{
		width: 76px;
		height: 76px;
	}
	.btn-file {
		position: relative;
		overflow: hidden;
	}
	.btn-file input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		min-width: 100%;
		min-height: 100%;
		font-size: 100px;
		text-align: right;
		filter: alpha(opacity=0);
		opacity: 0;
		outline: none;
		background: white;
		cursor: inherit;
		display: block;
	}
	.ead-file-btn{
		color: #3e87dd;
		/* background-color: #fff; */
		border-color: #3e87dd;
		font-size: 1em;
		float: none;
		padding: 8px 33px;
		border: 2px solid;
		border-radius: 7px;
		font-family: 'Open Sans', sans-serif;
		transition: 0.5s all;
		-ms-transition: 0.5s all;
		-o-transition: 0.5s all;
		-webkit-transition: 0.5s all;

	}
	.ead-file-btn span{
		padding: 0px 12px 0px 0px;
	}
	.ead-file-btn:hover{
		color:#5ba6ff;
	}
	.ead-file--loading,.ead-file--state {
		width: 100%;
		height: 100%;
		position: absolute;
		background-color: rgba(237, 239, 245, 0.82)}
		.ead-file--loading input{
			display: none
		}

		.ead-file--state span {
			width: 15px;
			height: 12px;
			background: url('http://educacao.app/vendor/img/file/icons.png') no-repeat;
			position: absolute;
			top: 50%;
			left: 50%;
			cursor: pointer;
			transform: translate(-50%,-50%);
			-webkit-transform: translate(-50%,-50%);
			-ms-transform: translate(-50%,-50%);
			-o-transform: translate(-50%,-50%);
		}
		.ead-file--files.working .ead-file--state span {
			height: 16px;
			background-position: 0 -12px;
		}
		.ead-file--remove{

			border: 2px solid #F65177;
		}
		.ead-file--delete{
			position: fixed;
			bottom: 4%;
			right: 2%;
			width: 75px;
			height: auto;

    cursor: pointer;
		}
		.ead-file--tresh{fill:#2f323a;opacity: 0}
		.ead-file-tresh-stroke{fill:none;stroke:#FFFFFF;stroke-width:4;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
		.ead-tresh--bucket,.ead-tresh--cover{opacity: 0}


.ead-file-list--User{

    margin-top: 50px;
}
	</style>
	<header>
		<div class="ead-nav">
			<ol class="ead-breadcrumb">
				<li class="ead-breadcrumb-item"><a href="#">Aliança de Minas	</a></li>
				<li class="ead-breadcrumb-item"><a href="{{route('user.panel')}}">Curso De Gestão Cultural</a></li>
				<li class="ead-breadcrumb-item active">Arquivos</li>

			</ol>
		</div>

	</header> 
	<div class="ead-row--ajust">

		<ul class="ead-file-list--User">

			@foreach($entries as $entry)

			<div class="col-xs-6 col-md-3">
				<div class="ead-file--files">
					<div class="ead-file--type col-md-3">
						<span class="hide id">{{$entry->id}}</span>
						<span class="ead-file-icon--font ead-icon-file-text2 ead-icon-file-{{$entry->extension}}"></span>
						<span class="ead-file-icon--text">{{$entry->size}} MB</span>

					</div>
					<div class="ead-file--information col-md-6 text-center">
						<p>{{$entry->original_filename}}</p>
					</div>

					<div class="col-md-3 ead-file--icon text-center">
						<a class="ead-file--download" href="get/{{$entry->filename}}" download='{{$entry->original_filename}}'>
							<span class="ead-file-icon--font ead-icon-download3"></span>
							<span class="ead-file-icon--text">Baixar</span></a>

						</div>
					</div>
				</div>	


				@endforeach
			</ul>



	</div>
	@push('scripts')

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

	@endpush

	@endsection
