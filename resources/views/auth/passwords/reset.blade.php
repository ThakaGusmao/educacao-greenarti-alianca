@extends('layouts.app')

@section('content')

<style type="text/css">

.ead-login-container{
    color: #58666e;
    margin: auto;
    position: absolute;
    top: 0; 
    left: 0;
    bottom: 0;
    right: 0;
    width: 386px;
    height: 60%;
}
.ead-login-container{
    color: #58666e;
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    width: 386px;
    height: 40%;
}
.ead-logo{
    color: #3398DB;
    margin-bottom: 30px;
    font-size: 22px;
}
.ead-logo-img{
    width: 128px;
}
.ead-logotipo{
    width: 258px 
}
.form-control-custom {

    outline: none;
    display: block;
    width: 100%;
    height: 44px;
    padding: 6px 49px;
    font-size: 20px;
    line-height: 1.42857143;
    color: #545353;
    background-color: transparent;
    background-image: none;
    border: 0px;
    border-bottom: 1px solid #797e8a;
    /* border-radius: 4px; */
    /* -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075); */
    /* box-shadow: inset 0 1px 1px rgba(0,0,0,.075); */
    /* -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s; */
/*    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
*/    /* transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s; */
}

.form-group{
        position: relative;
        margin-bottom: 35px;
}

.form-group span{
        position: absolute;
    /* top: 0%; */
    bottom: 10px;
    left: 6px;
    color: #3398db;
    font-size: 28px;
}
    .btn-questions{
    background-color: #2C7EDC;
    margin-top: -2px;
    border-radius: 26px;
    outline: none;
    padding: 17px 10px;
    border-color: transparent;
    transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -o-transition: all 0.5s;
    -webkit-transition: all 0.5s;
    outline: none;
    font-size: 1.2em;
    font-weight: bold;
    position: relative;


    }
    .btn-questions:hover{
        color: #fff;
        background-color: #66acfd;
        border-color: #ffffff;
        outline: none


    }
    .btn-questions:active,.btn-questions:focus{
        color: #fff;
        background-color: #2e64a5;
        border-color: #ffffff;
        outline: none

    }
        .btn-questions--lg{
         margin-bottom: 5px;
    width: 67%;
    border-radius: 36px;

    }
    .btn-questions::after{
        content: ">";
        position: absolute;
        right: 15%;
          transition: all 0.5s;
    -ms-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -o-transition: all 0.5s;
    -webkit-transition: all 0.5s;
    }

    .btn-questions:hover::after{
    
        transform: translateX(20px);
    }

.help-block {
    display: block;
    margin-top: 5px;
    margin-bottom: 10px;
    color: #d65959;
    text-align: right;
}

.form-control-custom.disabled{
	cursor: not-allowed;
}

    </style>
    

<div class="ead-login-container" id="app">
 <div class="ead-logo"><img class="ead-logo-img" src="{{asset('./vendor/img/default/logo_img.png')}}"><img class="ead-logotipo" src="{{asset('./vendor/img/default/logotipo.svg')}}"></div>                    <form class="form-horizontal" method="POST" action="/password/reset">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">
                       
				        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
				            <span class="ead-icon-user"></span>
				            
				            <input type="email" name="email" class="form-control-custom disabled"  placeholder="Email" required value="{{ $email or old('email') }}">

                       


                        </div>
         		
                        <div class="form-group">
						        <span class="ead-icon-key"></span>
						    		<input name="password" type="password"  v-model="password" class="form-control-custom" placeholder="Senha" required >

                  
                        </div>
             		
                               <div class="form-group">
						        <span class="ead-icon-key"></span>
						    		<input name="password_confirmation" @keyup="passwordValidation" v-model="passwordMatch" type="password" class="form-control-custom" placeholder="Senha Confirmação" required>

                    
                            </div>

						@if ($errors->has('senha'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('senha') }}</strong>
                                    </span>
                                @endif
								<span class="help-block" v-if="!passwordConfirmation">
                                        <strong>Senhas não coincidem!</strong>
                                    </span>

                               <div class="col-xs-12 text-center">
        		        <button type="submit" :disabled="!passwordConfirmation" :class="{'disabled' : !passwordConfirmation}" class="btn btn-lg btn-info btn-questions btn-questions--lg">Enviar</button>
            </div>
                    </form>
                </div>
            </div>
        </div>
   </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
	<script src="{{ asset('/vendor/js/vendor/vue/vue-resource.min.js') }}"></script>
   <script type="text/javascript">
   	
    Vue.config.devtools = true;

   	new Vue({
   		el:"#app",
   		data:{
   			passwordConfirmation: false,
   			passwordMatch: "",
   			password:""
   		},
   		methods:{

   			passwordValidation: function () {
	   		
   				if(this.passwordMatch === this.password){
		   			this.$set('passwordConfirmation',true);

   			}else{
   				this.$set('passwordConfirmation',false);

   			}
   		}
   	}
   	});



   </script>
@endsection
