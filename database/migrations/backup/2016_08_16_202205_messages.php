<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('teacher_id')->references('id')->on('user');
            $table->string('title');
            $table->string('description');
            $table->string('response');
            $table->integer('response_true');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
