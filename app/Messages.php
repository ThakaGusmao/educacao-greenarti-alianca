<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class messages extends Model
{
    protected $table = 'messages';
    protected $fillable = ['title','description', 'response', ''];
    protected $casts = [
            'response_true' => 'boolean',
    ];
      protected $hidden = [
        'teacher_id', 'user_id'
    ];

}
