<?php

namespace App\Http\Controllers\User;

use App\Files;
use App\Http\Controllers\Controller;
use Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

class FilesUser extends Controller
{
    //

public function listFiles()
	{
		$entries = Files::all();
		return view('User/files', compact('entries'));
	}
	


	public function get($filename){
	
		$entry = Files::where('filename', '=', $filename)->firstOrFail();
 
		$file = Storage::disk('local')->get('GestaoCultural/' . $entry->filename);
		  ob_end_clean();
		 return response($file)->header('Content-Type', $entry->mime);

	}



}
