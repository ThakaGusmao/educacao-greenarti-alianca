<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Domain\RepositoryAll\VideoStream as vStream;
use Debugbar;

class MainUser extends Controller
{



	public function panel()
	{

		$activeVideo = 1;

		$filePath = storage_path().'/app/GestaoCultural/videos/Modulo_Educacao_Patrimonial.mp4';
		$erros = file_exists($filePath);


		if($erros)$nameVid = encrypt("Modulo_Educacao_Patrimonial.mp4");
		return view('User.principal',compact('activeVideo','nameVid'))->witherros($erros);

	}

		public function panelVideo($name)
		{
			$activeVideo = 2;

		    $filePath = storage_path().'/app/GestaoCultural/videos/Modulo_Politica_Cultural.mp4';
			$erros = file_exists($filePath);
	
			if($erros)$nameVid = encrypt("Modulo_Politica_Cultural.mp4");

			return view('User.principal',compact('activeVideo','nameVid'))->witherros($erros);
		}


	public function getVideo($name)
			{

			$referer = Request::server('HTTP_REFERER');

			if( $referer != null){
				
				$name = decrypt($name);

			    $filePath = storage_path().'/app/GestaoCultural/videos/'.$name;
			    ob_end_clean();

		        $stream = new vStream($filePath);
		        return response()->stream(function() use ($stream) {

		            $stream->start();

		        });

			}else{
				return abort(403, 'Arquivo não encontrado');

			}

		}




}

