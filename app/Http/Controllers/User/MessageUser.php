<?php

namespace App\Http\Controllers\User;

use Request;

use App\Http\Controllers\Controller;
use App\Messages;
use Auth;
use Mail;
use App\User;
use Domain\RepositoryAll\ofuscateRepository as ofuscate;



class MessageUser extends Controller
{
	protected $ofuscate;


	public function __construct(ofuscate $ofuscateRepo)
	{
		$this->ofuscate = $ofuscateRepo->register();
	}


    public function message()
    {
		return view('User/message');
    }


	public function messageList()
	{
		$userId = Auth::user()->id;
		$questions = Messages::select('messages.*', 'teachers.name')->join('teachers', 'messages.teacher_id', '=', 'teachers.id')->groupBy('messages.id')->where('user_id', '=', $userId)->get();
		
		foreach ($questions as $quetion) {
			$quetion->id =$this->ofuscate->encode($quetion->id);
		}

		return response()->json($questions);

	}

    
	public function addMessage()
	{

		$userId = Auth::user()->id;
		$userEmail = Auth::user()->email;
		$request = Request::all();
		$question = new Messages();

		$question->title = $request['title'];
		$question->description = $request['description'];
		$question->teacher_id = $request['teacher'];
		$question->response_true = 0;
		$question->user_id = $userId;


		$question->save();

		$request['titleQuestion'] = 'A sua pergunta foi efetuada com êxito.';
		$request['footerQuestion'] = 'Em breve será respondida. Obrigado!';
		$request['admin'] = false;


		Mail::send('emails.emailSendMessage', ['data' => $request], function ($message) use ($userEmail) {
		    $message->from('educacao@educacaoaliancademinas.com.br', 'Educacao | Alianca de Minas');
		    $message->to($userEmail)->bcc('processosdigitais@greenarti.com');
		    $message->subject('Pergunta realizada com sucesso !');
		});


		$request['titleQuestion'] = 'Professor, você possui uma pergunta a ser respondida.';
		$request['footerQuestion']  = 'Não deixe de responder.';
		$request['admin'] = true;

		$adminEmail = User::select('users.email','teachers.id_user','teachers.id')->join('teachers','teachers.id_user','=','users.id')
		->where('teachers.id','=',$request['teacher'])->get()->first();

		Mail::send('emails.emailSendMessage', ['data' => $request], function ($message) use($adminEmail) {
		    $message->from('educacao@educacaoaliancademinas.com.br', 'Educacao | Alianca de Minas');
		    $message->to($adminEmail->email)->bcc('processosdigitais@greenarti.com');
		    $message->subject('Alerta de pergunta realizada!');
		});



		return redirect('user/mensagens');

	}

	public function messageId($id)
	{
		$idDecode = $this->ofuscate->decode($id);
		return view('User/messageView',compact($idDecode));
	}

	public function email()
	{
		
		Auth::loginUsingId(5);
		$data = Messages::find(1);
		$data['admin'] = false;
		$data['titleQuestion'] = "A sua pergunta foi efetuada com êxito.";
		dd(Auth::loginUsingId(3));

		$adminEmail = User::select('users.email','teachers.id_user','teachers.id')->join('teachers','teachers.id_user','=','users.id')
		->where('teachers.id','=','2')->get()->first();
		$data['response']='Teste teste ';


		return view('emails/emailSendMessage',compact('data'));


	}

}
