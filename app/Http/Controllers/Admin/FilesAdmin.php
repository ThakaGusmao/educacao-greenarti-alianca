<?php

namespace App\Http\Controllers\Admin;
use App\Files;
use App\Http\Controllers\Controller;
use Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

class FilesAdmin extends Controller
{
    //

	public function listFiles()
	{
		$entries = Files::all();
	
		return view('Admin/files', compact('entries'));
	}

	public function add() {

		$file = Request::file('upl');

		$extension = $file->getClientOriginalExtension();


		if(collect(Storage::directories())->has(("GestaoCultural"))){
					Storage::makeDirectory("GestaoCultural");
		};

		Storage::disk('local')->put("GestaoCultural/".$file->getFilename().'.'.$extension, File::get($file));

		$entry = new Files();
		$entry->mime = $file->getClientMimeType();
		$entry->extension = $file->getClientOriginalExtension();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename().'.'.$extension;
		$entry->size = number_format($file->getClientSize() / 1048576, 2);

		$entry->save();

		return redirect('admin/arquivos');
		
	}

		public function get($filename){
	
		$entry = Files::where('filename', '=', $filename)->firstOrFail();
 
		$file = Storage::disk('local')->get('GestaoCultural/' . $entry->filename);
		ob_end_clean();
		return response($file)->header('Content-Type', $entry->mime);

		}

		public function deleteFiles(){

		$dataDelete = Request::json()->all();
 		
 		if(empty($dataDelete)){
            return response()->json(array('success' => false));
 		};
 		foreach ($dataDelete as $file) {
 			
 				
 				
 				$objModel = Files::find($file['id']);
				Storage::disk('local')->delete('GestaoCultural/' . $objModel->filename);
				$objModel->delete();	
 		}


				 return response()->json(array('success' => true));


		}
 		


}
