<?php

namespace App\Http\Controllers\Admin;

use Request;

use App\Http\Controllers\Controller;
use App\Messages;
use Auth;
use DB;
use Mail;
use App\User;
use Domain\RepositoryAll\ofuscateRepository as ofuscate;



class MessageAdmin extends Controller
{
	protected $ofuscate;


	public function __construct(ofuscate $ofuscateRepo)
	{
		$this->ofuscate = $ofuscateRepo->register();
	}


    public function message()
    {
		return view('Admin/message');
    }


	public function messageList()
	{
		
		$userId = Auth::user()->id;

    	if(Auth::user()->type==2){
		$teacher_id = DB::table('teachers')->select('id')->where('id_user', $userId)->first();
		
		if(!$teacher_id){
			return response()->json();
		}else{
			$questions = Messages::select('messages.*', 'teachers.name' ,'users.name as student')->join('teachers', 'messages.teacher_id', '=', 'teachers.id')->join('users','messages.user_id','=','users.id')->groupBy('messages.id')->where('teacher_id', '=', $teacher_id->id)->get();	

		}


    	}else{

    		$questions = Messages::select('messages.*', 'teachers.name')->join('teachers', 'messages.teacher_id', '=', 'teachers.id')->groupBy('messages.id')->get();	
    	}

		foreach ($questions as $quetion) {
			$quetion->id =$this->ofuscate->encode($quetion->id);
		}

		return response()->json($questions);

	}

    
   public function updateMessage()
	{
		$id =Request::input('id');
		$idDecode = $this->ofuscate->decode($id);

		$response = Request::input('response');
		$question = Messages::find($idDecode);

		$question->response = $response;
		$question->response_true = 1;

		$request['user_id'] = $question->user_id;
		$request['title'] = $question->title;
		$request['description'] = $question->description;
		$request['response'] = $question->response;


		$question->save();

		$request['titleQuestion'] = 'Pergunta respondida.';
		$request['footerQuestion']  = 'Quaisquer novas dúvidas entre em contato.';
		$request['admin'] = false;

		$userEmail = User::find($request['user_id'])->get()->first();

		Mail::send('emails.emailSendMessage', ['data' => $request], function ($message) use($userEmail) {
		    $message->from('educacao@educacaoaliancademinas.com.br', 'Educacao | Alianca de Minas');
		    $message->to($userEmail->email)->bcc('processosdigitais@greenarti.com');
		    $message->subject('Alerta de pergunta respondida!');
		});

		return redirect('admin/mensagens');
	}



}
