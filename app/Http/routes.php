    <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

    Route::get('/', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
    Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
    Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
    Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

    //  Registration Routes...
    //  Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
    //  Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@register']);

    // Password Reset Routes...
    
    Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
    Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
    Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);



    Route::group(['middleware' => 'auth'], function () {
        
        Route::get('data/{name}',['as' => 'getVideo','uses' => 'Admin\MainAdmin@getVideo']);

        Route::group(['prefix' => 'user'], function () {

               Route::get('painel', [
                'as' => 'user.panel', 'uses' => 'User\MainUser@panel'
                ]);
   Route::get('painel/{name}', [
                    'as' => 'user.panelVideo', 'uses' => 'User\MainUser@panelVideo'
                    ]);
            Route::get('arquivos', [
                'as' => 'user.files', 'uses' => 'User\FilesUser@listFiles'
                ]);

            Route::get('get/{filename}', [
                'as' => 'user.getentry', 'uses' => 'User\FilesUser@get'
                ]);
                
            Route::get('mensagens',[
                'as' => 'user.message', 'uses' => 'User\MessageUser@message'
                ]);

            Route::get('mensagenslista',[
                'as' => 'user.messageList', 'uses' => 'User\MessageUser@messageList'
                ]);

            Route::get('mensagens/{id}',[
                'as' => 'user.messageId', 'uses' => 'User\MessageUser@messageId'
                ]);

            Route::post('addmessage',[
                'as' => 'user.addmessage', 'uses' => 'User\MessageUser@addMessage'
            ]);
            
        });

        Route::group(['prefix' => 'admin','middleware' => 'checkAdmin'], function(){

            // Route::group(['prefix' => 'admin'], function(){

                Route::get('painel', [
                    'as' => 'admin.panel', 'uses' => 'Admin\MainAdmin@panel'
                    ]);
                Route::get('painel/{name}', [
                    'as' => 'admin.panelVideo', 'uses' => 'Admin\MainAdmin@panelVideo'
                    ]);
                Route::get('arquivos', [
                    'as' => 'admin.files', 'uses' => 'Admin\FilesAdmin@listFiles'
                    ]);
                Route::post('add',[ 
                    'as' => 'admin.addentry', 'uses' => 'Admin\FilesAdmin@add'
                    ]);
                Route::get('get/{filename}', [
                    'as' => 'admin.getentry', 'uses' => 'Admin\FilesAdmin@get'
                    ]);
                Route::post('delete', [
                    'as' => 'admin.deleteentry', 'uses' => 'Admin\FilesAdmin@deleteFiles'
                    ]);
                Route::get('mensagens',[
                    'as' => 'admin.message','uses' => 'Admin\MessageAdmin@message'
                    ]);
            	Route::get('mensagenslista',[
                'as' => 'admin.messageList', 'uses' => 'Admin\MessageAdmin@messageList'
                ]);
                Route::post('updatemessage',[
                'as' => 'admin.updatemessage', 'uses' => 'Admin\MessageAdmin@updateMessage'
                ]);

            	
        });

	});
    

